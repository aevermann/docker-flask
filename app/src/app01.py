from flask import Flask
hello = Flask(__name__)
 
@hello.route("/")
def greeting():
    return "<h1 style='color:black'>InitBox UG Flask Docker</h1>"
 
if __name__ == "__main__":
    hello.run(host='0.0.0.0')
